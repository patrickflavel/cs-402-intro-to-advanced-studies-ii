/* Author: Patrick Flavel
 * Class: CS 402
 * Instructor: Virgil Bistriceanu
 * C Lab#: 2
 * Date: 4/20/2016
 * Name of Program: CLab2.c
*/

/* This program outputs statistical information about the contents of an input file.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <Math.h>
#include "CLab2.h"

//reads input file and stores into an array of floats, which is returned
float *load_file_into_array(char fileName[], float data[], FILE *fp, int *capacity, int *size) {
    //read file
    fp = fopen(fileName, "r");

    //if file pointer null, prompt & exit
    if (fp == NULL) {
        printf("\nThe input file could not be opened.");
        exit(1);
    }
    //otherwise, read file
    else {
        char *input_string = malloc(60 * sizeof(char));
        //if nothing is in the file, prompt & exit
        if (fgets(input_string, 60, fp) == NULL) {
            printf("\nThis file is empty");
            exit(1);
        }

        //read file, line by line, delimited by '\n' or '\r'
        rewind(fp);
        while (fgets(input_string, 60, fp)) {
            //if char array contains '\n' or '\r' then this is the end of the file, so break from the while loop
            if (input_string[0] == '\n' || input_string[0] == '\r') {
                break;
            }
            //otherwise, store the float in the data array
            else {
                char *value;
                //removes '\n' from the end of the char array
                value = strtok(input_string, "\n");

                //if the number of elements is greater than or equal to the capacity of the data array, then grow the capacity of the array
                if (*size >= *capacity) {
                    data = grow(data, capacity);
                }
                //string to float, and store in the data array
                data[*size] = strtof(value, NULL);
                //size represents the number of elements stored in the array (i.e. NOT the capacity)
                *size = *size + 1;
            }
        }
        fclose(fp);
        return data;
    }
}

//grow array to be double the size of the input array
float *grow(float data[], int *capacity) {
    *capacity = 2 * (*capacity);
    float *_data = (float *) calloc((size_t) *capacity, sizeof(float));
    for (int i = 0; i < *capacity/2; i++) {
        _data[i] = data[i];
    }
    free(data);
    return _data;
}

//calculates the mean of an array of floats
double mean(float data[], int *size) {
    double sum = 0;
    for (int i = 0; i < *size; i++) {
        sum = sum + data[i];
    }
    double mean = sum / *size;
    return mean;
}

//calculates the median of an array of floats
double median(float data[], int *size) {

    //sorts the data array into ascending order
    for (int i = 0; i < *size; ++i) {
        for (int j = i + 1; j < *size; ++j) {
            if (data[i] > data[j]) {
                float a =  data[i];
                data[i] = data[j];
                data[j] = a;
            }
        }
    }

    //determines index/indices of array that represent the median/s, then calculates and returns the median
    int index1, index2;
    double median = 0;
    if (*size % 2 == 0) {
        index1 = *size / 2 - 1;
        index2 = *size / 2;
        median = (data[index1] + data[index2]) / 2;
    }
    else if (*size % 2 == 1) {
        index1 = *size / 2;
        median =  data[index1];
    }
    return median;
}

//calculates the population standard deviation of an array of floats
double standard_deviation(float data[], int *size) {

    double sum = 0;
    double average = mean(data, size);

    for (int i = 0; i < *size; i++) {
        double difference = data[i] - average;
        double difference_squared = difference * difference;
        sum = sum + difference_squared;
    }

    double stddev = sqrt(sum / *size);
    return stddev;
}

//output the statistical results to the console
void presentResults(double average, double med, double stddev, int* size, int* capacity) {

    char msg1[20] = "\nNum values:";
    char msg2[20] = "\nmean:";
    char msg3[20] = "\nmedian:";
    char msg4[20] = "\nstddev:";

    printf("\nResults:");
    printf("\n--------");
    printf("%-12s %20d", msg1, *size);
    printf("%-12s %20f", msg2, average);
    printf("%-12s %20f", msg3, med);
    printf("%-12s %20f", msg4, stddev);
    printf("\nUnused array capacity: %9d", (*capacity - *size));
    printf("\n");

}

//main application function
int main(int argc, char *argv[]) {

    FILE *fp = NULL;

    //define initial size and capacity of the data array
    int *size, *capacity;
    int initial_capacity = 20, initial_size = 0;
    capacity = &initial_capacity;
    size = &initial_size;

    //allocate memory for an array of floats
    float *data;
    data = malloc(*capacity * (sizeof(float)));

    //read file and store into the data array of floats
    data = load_file_into_array(argv[1], data, fp, capacity, size);

    //calculate statistical values
    double average = mean(data, size);
    double med = median(data, size);
    double stddev = standard_deviation(data, size);

    //output results to console
    presentResults(average, med, stddev, size, capacity);

    exit(0);
}