Author: Patrick Flavel
CWID: A20355341
Email: pflavel@hawk.iit.edu
Class: CS 402
Instructor: Virgil Bistriceanu
C Lab#: 2
Date: 4/21/2016 (USING 1 DAY OF LATE CREDIT FOR THIS LABORATORY ASSIGNMENT)


Compile CLab2.c and CLab2.h to generate an executable. Run that executable through the command line (or run basicstats.out, which has been compiled for you). Either executable will require 1 input variable, being the name of the input file which must be located in the same folder as the executable and c files (e.g. small.txt).