//
// Created by Patrick Flavel on 18/04/2016.
//

#ifndef CS402_C_LAB2_CLAB2_H
#define CS402_C_LAB2_CLAB2_H

float *load_file_into_array(char[], float[], FILE *, int *, int *);
float *grow(float[], int *);
double mean(float[], int *);
double median(float[], int *);
double standard_deviation(float[], int *);
void presentResults(double, double, double, int*, int*);

#endif //CS402_C_LAB2_CLAB2_H