/* Author: Patrick Flavel
 * Class: CS 402
 * Instructor: Virgil Bistriceanu
 * Homework#: 1
 * Date: 2/1/2016
 * Name of Program: Matrix_Multiplication
*/

/* This program multiplies non-square matrices with either (exclusively) integer or double variables as elements.
 */

package homework1_CS402;

import java.util.Random;

class Matrix_Multiplication {

	public static void main(String[] args) {

		final double NANO_FACTOR = 1000000000.0;  // nanoseconds per second

		long startTime = 0,
		     finishTime = 0,
		     elapsedTime,
		     total = 0;
		
		System.out.println("Matrix multiplication with integer variable matrices:");
		//perform the matrix multiplication on the integer variable matrices and calculate the elapsed time
		for (int i = 0; i < 10; i++) {
			startTime = System.nanoTime();
			multiply_int();
			finishTime = System.nanoTime();
			elapsedTime = finishTime - startTime;
			total += elapsedTime;
			System.out.println ("The elapsed time was " + (elapsedTime / NANO_FACTOR) + " seconds.");
		}
		//average time
		System.out.println ("Therefore, the average time to perform the matrix multiplication (with integer variables) is " + total/10.0/NANO_FACTOR + " seconds.");

		//reinitialise timing variables
		startTime = 0;
		finishTime = 0;
		elapsedTime = 0;
		total = 0;
		
		System.out.println("\n\nMatrix multiplication with double variable matrices:");
		//perform the matrix multiplication on the double variable matrices and calculate the elapsed time
		for (int i = 0; i < 10; i++) {
			startTime = System.nanoTime();
			multiply_double();
			finishTime = System.nanoTime();
			elapsedTime = finishTime - startTime;
			total += elapsedTime;
			System.out.println ("The elapsed time was " + (elapsedTime / NANO_FACTOR) + " seconds.");
		}
		//average time
		System.out.println ("Therefore, the average time to perform the matrix multiplication (with double variables) is " + total/10.0/NANO_FACTOR + " seconds.");

	}

	public static void multiply_int() {
		
		int Arows = 900, Acols = 800;
		int Brows = 800, Bcols = 1000;
		int[][] A = new int[Arows][Acols];
		int[][] B = new int[Brows][Bcols];
		
		Random random = new Random();
		
		for (int i = 0; i < Arows; i++) {
			for (int j = 0; j < Acols; j++) {
				A[i][j] = random.nextInt();
			}
		}
		
		for (int i = 0; i < Brows; i++) {
			for (int j = 0; j < Bcols; j++) {
				B[i][j] = random.nextInt();
			}
		}
		
		int[][] C = new int[Arows][Bcols];
		for (int i = 0; i < Arows; i++) {
			for (int j = 0; j < Bcols; j++) {
				C[i][j] = 0;
			}
		}
		
		for (int i = 0; i < Arows; i++) {
			for (int j = 0; j < Bcols; j++) {
				for (int k = 0; k < Acols; k++) {					//the limit to k is either Acols or Brows (these two values must be equivalent for matrix multiplication)
					C[i][j] += A[i][k]*B[k][j];
				}
			}
		}
	}
	
public static void multiply_double() {
		
		int Arows = 900, Acols = 800;
		int Brows = 800, Bcols = 1000;
		double[][] A = new double[Arows][Acols];
		double[][] B = new double[Brows][Bcols];
		
		Random random = new Random();
		
		for (int i = 0; i < Arows; i++) {
			for (int j = 0; j < Acols; j++) {
				A[i][j] = random.nextDouble();
			}
		}
		
		for (int i = 0; i < Brows; i++) {
			for (int j = 0; j < Bcols; j++) {
				B[i][j] = random.nextDouble();
			}
		}
		
		int[][] C = new int[Arows][Bcols];
		for (int i = 0; i < Arows; i++) {
			for (int j = 0; j < Bcols; j++) {
				C[i][j] = 0;
			}
		}
		
		for (int i = 0; i < Arows; i++) {
			for (int j = 0; j < Bcols; j++) {
				for (int k = 0; k < Acols; k++) {					//the limit to k is either Acols or Brows (these two values must be equivalent for matrix multiplication)
					C[i][j] += A[i][k]*B[k][j];
				}
			}
		}
	}
	
}
