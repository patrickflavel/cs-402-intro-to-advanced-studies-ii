Author: Patrick Flavel
CWID: A20355341
Class: CS 402
Instructor: Virgil Bistriceanu
Lab#: 1
Date: 2/15/2016


Please see Lab1_Report.docx for answers to all questions.


***Pre-Lab***
Please see Lab1.1.2.asm and Lab1.1.7.asm.


***In-Lab***
Please see Lab1.2.asm.


***Post-Lab***
Please see Lab1.3.asm and Lab1.4.asm.