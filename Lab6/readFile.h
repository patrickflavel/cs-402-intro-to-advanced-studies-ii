/* Author: Patrick Flavel
 * Class: CS 402
 * Instructor: Virgil Bistriceanu
 * C Lab#: 1
 * Date: 4/15/2016
 * Name of Program: readFile.h
*/

/* This header file contains all declarations of functions in readFile.c, as well as the declaration of struct Employee.
 */

#ifndef CS402_C_LAB1_READFILE_H
#define CS402_C_LAB1_READFILE_H

#endif //CS402_C_LAB1_READFILE_H

struct Employee {
    char firstName[64];
    char lastName[64];
    int ID;
    int salary;
} Employee;

int open_file(char[], struct Employee[], FILE *);
void menu();
int read_int_console(int *);
int read_int_console_check_type_bounds(char *, int, int);
int read_string_console(char[]);
int read_int_file(FILE *, int x);
int read_string_file(FILE *, char[]);
int read_float_file(FILE *, float);
void job(int, struct Employee[]);
void print_database(struct Employee[]);
int size_of_employees_array(struct Employee[]);
int search_by_ID(struct Employee[], int);
int array_of_IDs(struct Employee[], int *);
int search_by_lastName(struct Employee[], char *, int, int *, int *);
int add_employee_to_array(struct Employee *);