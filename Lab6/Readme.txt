Author: Patrick Flavel
CWID: A20355341
Class: CS 402
Instructor: Virgil Bistriceanu
C Lab#: 1
Date: 4/15/2016


Compile readFile.c and readFile.h to generate an executable. Run the executable through the command line. The executable will require 1 input variable, being the name of the input file which must be located in the same folder as the executable and c files (i.e. input.txt).