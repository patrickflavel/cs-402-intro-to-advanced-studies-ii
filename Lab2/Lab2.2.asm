.data 0x10010000
var1: .word 0x55	# var1 is a word (32 bit) with the initial value 0x55
var2: .word 0xaa 	# var2 is a word (32 bit) with the initial value 0xaa
var3: .word 0x33	# var3 is a word (32 bit) with the initial value 0x33
var4: .word 0xcc 	# var4 is a word (32 bit) with the initial value 0xcc
first: .byte 'p'	# first letter of first name
last: .byte 'f'	 	# first letter of last name

.text
.globl main
main: addu $s0, $ra, $0	# save $31 in $16

lw $t0, var1		# load word var1 in $t0
lw $t1, var2		# load word var2 in $t1
lw $t2, var3		# load word var3 in $t2
lw $t3, var4		# load word var4 in $t3
la $t4, first		# load first in $t4
la $t5, last		# load last in $t5

move $t6, $t0		# move value of var1 to $t6
move $t7, $t3		# move value of var4 to $t7
move $t0, $t7		# move value of $t7 (var4) to $t0 (var1)
move $t3, $t6		# move value of $t6 (var1) to $t3 (var4)
move $t6, $t1		# move value of var2 to $t6
move $t7, $t2		# move value of var3 to $t7
move $t1, $t7		# move value of $t7 (var3) to $t1 (var2)
move $t2, $t6		# move value of $t6 (var2) to $t2 (var3)

			# restore now the return address in $ra and return from main
addu $ra, $0, $s0 	# return address back in $31
jr $ra 			# return from main
