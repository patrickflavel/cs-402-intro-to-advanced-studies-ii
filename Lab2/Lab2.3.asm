.data 0x10010000
var1: .word 0x55 	# var1 is a word (32 bit) with the initial value 0x55
var2: .word 0xaa 	# var2 is a word (32 bit) with the initial value 0xaa
var3: .word 0x33 	# var3 is a word (32 bit) with the initial value 0x33
var4: .word 0xcc 	# var4 is a word (32 bit) with the initial value 0xcc
first: .byte 'p' 	# first letter of first name
last: .byte 'f'	 	# first letter of last name

.text
.globl main
main: addu $s0, $ra, $0	# save $31 in $16

lui $1, 4097		# load upper half of 4097 address (0x10010000) in $1
lw $8, 0($1)		# displace $1 by 0 bytes and place in $8

lui $1, 4097		# load upper half of 4097 address (0x10010000) in $1
lw $9, 4($1)		# displace $1 by 4 bytes and place in $9

lui $1, 4097		# load upper half of 4097 address (0x10010000) in $1
lw $10, 8($1)		# displace $1 by 8 bytes and place in $10

lui $1, 4097		# load upper half of 4097 address (0x10010000) in $1
lw $11, 12($1)		# displace $1 by 12 bytes and place in $11

lui $1, 4097		# load upper half of 4097 address (0x10010000) in $1
ori $12, $1, 0x10	# ORI behaves like unsigned addition: $12 <- $1 + 0x10

lui $1, 4097		# load upper half of 4097 address (0x10010000) in $1
ori $13, $1, 0x11	# ORI behaves like unsigned addition: $13 <- $1 + 0x11

addu $14, $0, $8	# add $8 to $0 (zero) and place in $14
addu $15, $0, $11	# add $11 to $0 (zero) and place in $15
addu $8, $0, $15	# add $5 to $0 (zero) and place in $8
addu $11, $0, $14	# add $14 to $0 (zero) and place in $11
addu $14, $0, $9	# add $9 to $0 (zero) and place in $14
addu $15, $0, $10	# add $10 to $0 (zero) and place in $15
addu $9, $0, $15	# add $15 to $0 (zero) and place in $9
addu $10, $0, $14	# add $14 to $0 (zero) and place in $10

			# restore now the return address in $ra and return from main
addu $ra, $0, $s0 	# return address back in $31
jr $ra 			# return from main

ori $v0, $0, 10		# exits program
syscall
