Author: Patrick Flavel
CWID: A20355341
Class: CS 402
Instructor: Virgil Bistriceanu
Lab#: 2
Date: 2/22/2016


Please see Lab2_Report.docx for answers to all questions.


***Pre-Lab***
Please see Lab2.1.asm.


***In-Lab***
Please see Lab2.2.asm.


***Post-Lab***
Please see Lab2.3.asm and Lab2.4.asm.