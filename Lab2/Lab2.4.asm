.data 0x10010000
var1: .word 12 		# var1 is a word (32 bit) with the initial value 0x55
var2: .word 34 		# var2 is a word (32 bit) with the initial value 0xaa

.extern ext1 4		# ext1 is a word (32 bit)
.extern ext2 4		# ext2 is a word (32 bit)

.text
.globl main
main: addu $s0, $ra, $0	# save $31 in $16

lw $t0, var1		# load word var1 in $t0
lw $t1, var2		# load word var2 in $t1
la $t2, 0($gp)		# load address of ext1
la $t3, 4($gp)		# load address of ext2 (4 bytes displacement from $gp)

move $t3, $t0		# move value of $t0 (var1) to $t3 (ext2)
move $t2, $t1		# move value of $t1 (var2) to $t2 (ext1)

			# restore now the return address in $ra and return from main
addu $ra, $0, $s0 	# return address back in $31
jr $ra 			# return from main
