      .data 0x10000000
word1: 0x89abcdef
.text
.globl main
main:
la $4, word1
lwl $t0, 0($4)
lwl $t1, 1($4)
lwl $t2, 2($4)
lwl $t3, 3($4)

jr $ra # return from main