      .data 0x10000000
.align 0
ch1:  .byte 'a' # reserve space for a byte
word1: .word 0x89abcdef # reserve space for a word
ch2: .byte 'b' # b is 0x62 in ASCII
word2: .word 0 # reserve space for a word
.text
.globl main
main:
lui $t0, 0x1000
lwr $t1, 1($t0)
lwl $t1, 4($t0)
lui $t2, 4096
ori $t2, $t2, 6
swl $t1, 1($t2)
swr $t1, 2($t2)

jr $ra # return from main
