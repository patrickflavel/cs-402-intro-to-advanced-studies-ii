#include <iostream>						//standard library
#include <stdlib.h>						//standard library

using namespace std;

int A(int x, int y) {
	if (x == 0) {
		return y + 1;
	}
	else if (y == 0) {
		return A(x - 1, 1);
	}
	else {
		return A(x - 1, A(x, y - 1));
	}
}

int main(int argc, char *argv[]) {

	int x, y;

	cout << "Please insert two non-negative numbers (x and y), which will be used as inputs to the Ackermann function." << endl;

	cin >> x;
	cin >> y;

	cout << A(x, y);

	system("pause");

	return 0;
}