      .data 0x10000000
msg1: .asciiz "Please enter an integer number: "
msg2: .asciiz "The result: "

.text
.globl main

main:
add $sp, $sp, -4	# must save $ra since there's a call
sw $ra, 4($sp)

jal input
			# move $a0, $a1
jal Factorial		# call ‘Factorial’ with no parameters

move $t1, $v0
li $v0, 4		# system call for print_str
la $a0, msg2		# address of string to print
syscall			
			# now get an integer from the user
li $v0, 1		# system call for print_int
addu $a0, $t1, $0	# move number to print in $a0
syscall

lw $ra, 4($sp)		# restore the return address in $ra
add $sp, $sp, 4
jr $ra			# return from main

# The procedure ‘input’ does not call any other procedure. Therefore $ra
# does not need to be saved. Since ‘test’ uses no registers there is
# no need to save any registers.

input:
subu $sp, $sp, 4
sw $ra, 4($sp)

li $v0, 4		# system call for print_str
la $a0, msg1		# address of string to print
syscall			# now get an integer from the user

li $v0, 5		# system call for read_int
syscall			# the integer placed in $v0
			# do some computation here with the number

addu $t0, $v0, $0	# move the number into $t0
move $a1, $t0		# move the number into $a1
bgez $a1, input1	# if ($a1 >= 0) then branch to input1
jal input		# otherwise (i.e. if the input is negative), loop back to the start of 'input'

lw $ra, 4($sp)		# restore the return address in $ra
addu $sp, $sp, 4	# adjust the stack pointer
jr $ra

input1:
move $a0, $a1		# move input to $a0
lw $ra, 4($sp)		# restore the return address (for 'input') in $ra
addu $sp, $sp, 4	# adjust the stack pointer
jr $ra

Factorial:
subu $sp, $sp, 4
sw $ra, 4($sp)		# save the return address on stack
beqz $a0, terminate	# test for termination: if ($a0==0), then branch to 'terminate'
subu $sp, $sp, 4	# otherwise, do not terminate yet
sw $a0, 4($sp)		# save the parameter
sub $a0, $a0, 1		# subtract argument by 1
jal Factorial		# after the termination condition is reached, these lines will be executed

lw $t0, 4($sp)		# the argument I have saved on stack
mul $v0, $v0, $t0	# do the multiplication
lw $ra, 8($sp)		# prepare to return
addu $sp, $sp, 8	# popped 2 words (an address and an argument)
jr $ra

terminate:
li $v0, 1		# 0! = 1 is the return value
lw $ra, 4($sp)		# get the return address
addu $sp, $sp, 4	# adjust the stack pointer
jr $ra			# return
