      .data 0x10000000

msg1: .asciiz "Please enter an integer number: "
msg2: .asciiz "Please enter another integer number: "
msg3: .asciiz "The largest  number: "

.text
.globl main

main: add $sp, $sp, -4	# must save $ra since there's a call
      sw $ra, 4($sp)

      li $v0, 4		# system call for print_str
      la $a0, msg1	# address of string to print
      syscall
      			# now get an integer from the user
      li $v0, 5		# system call for read_int
      syscall		# the integer placed in $v0 (do some computation)
      addu $t0, $v0, $0	# move the number in $t0

      li $v0, 4		# system call for print_str
      la $a0, msg2	# address of string to print
      syscall		
			# now get an integer from the user
      li $v0, 5		# system call for read_int
      syscall		# the integer placed in $v0 (do some computation)
      addu $t1, $v0, $0 # move the number in $t1

      move $a0, $t0	# move numbers into parameter registers
      move $a1, $t1

      jal largest	# call (or jump to) ‘largest'
      nop		# execute this after ‘largest’ returns

      lw $ra, 4($sp)	# restore the return address in $ra
      add $sp, $sp, 4	# move stack pointer 4 bytes positively (as the top of the stack has just been 'popped')
      jr $ra		# return from main

# The procedure ‘largest’ calls another procedure.
# Therefore $ra must be saved in a location that preserves its value.

largest: add $sp, $sp, -4	# save return address on the stack
         sw $ra, 4($sp)

         move $t0, $a0		# move numbers into temporary registers
         move $t1, $a1

         slt $t2, $t0, $t1	# if ($t0 < $t1) then $t2 = 1; else $t2 = 0
         bgtz $t2, output1	# if $t2 is greater than zero, branch to output1 ($a1 is largest)

				# otherwise, $a0 is largest
         li $v0, 4		# system call for print_str
         la $a0, msg3		# address of string to print
         syscall

         li $v0, 1		# system call for print_int
         addu $a0, $t0, $0	# move number to print in $a0
         syscall

         lw $ra, 4($sp)		# restore the return address in $ra (this is the $ra of 'largest')
         add $sp, $sp, 4	# move stack pointer 4 bytes positively (as the top of the stack has just been 'popped')
         jr $ra			# return from this procedure

# The procedure ‘output1’ does not call any other procedure.
# Therefore $ra does not need to be saved.
# 'output1' is called if $a1 is largest.

output1: move $t3, $a1		# move largest number to $t3

         li $v0, 4		# system call for print_str
         la $a0, msg3		# address of string to print
         syscall

         li $v0, 1		# system call for print_int
         addu $a0, $t3, $0	# move number to print in $a0
         syscall

         lw $ra, 4($sp)		# restore the return address in $ra (this is the $ra of 'largest')
         add $sp, $sp, 4	# move stack pointer 4 bytes positively (as the top of the stack has just been 'popped')
         jr $ra			# return from this procedure
