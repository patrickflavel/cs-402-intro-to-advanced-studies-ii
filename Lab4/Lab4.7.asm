      .data 0x10010000
msg1: .asciiz "Enter the x input value (non-negative) for the Ackermann function:"
msg2: .asciiz "Enter the y input value (non-negative) for the Ackermann function:"
msg3: .asciiz "The result: "

.text
.globl main

main:
	add $sp, $sp, -4	# must save $ra since there's a call
	sw $ra, 4($sp)

	jal x_input		# get x input
	jal y_input		# get y input

	move $a0, $t0		# move the number into $a0
	move $a1, $t1		# move the number into $a1

	jal Ackermann		# call ‘Ackermann’

	move $t1, $v0		# move result into $t1
	li $v0, 4		# system call for print_str
	la $a0, msg3		# address of string to print
	syscall

	li $v0, 1		# system call for print_int
	addu $a0, $t1, $0	# move number to print in $a0
	syscall

	li $v0, 10		# terminate program
	syscall

x_input:
	li $v0, 4		# system call for print_str
	la $a0, msg1		# address of string to print
	syscall			# now get an integer from the user
	li $v0, 5		# system call for read_int
	syscall			# the integer placed in $v0
	bltz $v0, x_input	# if ($v0 < 0) then branch to x_input
	addu $t0, $v0, $0	# move the number into $t0
	jr $ra			# return

y_input:
	li $v0, 4		# system call for print_str
	la $a0, msg2		# address of string to print
	syscall			# now get an integer from the user
	li $v0, 5		# system call for read_int
	syscall			# the integer placed in $v0
	bltz $v0, y_input	# if ($v0 < 0) then branch to y_input
	addu $t1, $v0, $0	# move the number into $t1
	jr $ra			# return

Ackermann:
	addi $t2, $t2, 1	# count number of recursive calls
	subu $sp, $sp, 8	# adjust the stack pointer
	sw $ra, 0($sp)		# save the return address on stack
	sw $s0, 4($sp)		# save previous $s0 on stack
	bgtz $a0, test_y	# test for termination: if (x>0), then branch to 'test_y'
	add $v0, $a1, 1		# otherwise, x==0, then return value is y + 1
	j finished		# jump to finished

test_y:
	bgtz $a1, x_y_positive	# test for: if (y>0), then branch to 'x_y_positive'
	sub $a0, $a0, 1		# subtract argument by 1 (x-1)
	li $a1, 1		# y = 1
	jal Ackermann		# calls 'Ackermann'
	j finished		# jump to finished
		
x_y_positive:		
	addu $s0, $a0, $0	# save x
	sub $a1, $a1, 1		# subtract y by 1 (y - 1)
	jal Ackermann		# calls 'Ackermann'
	sub $a0, $s0, 1		# subtract x by 1 (x - 1)
	move $a1, $v0		# move result from 'Ackermann' call to y 
	jal Ackermann		# calls 'Ackermann'

finished:
	lw $s0, 4($sp)		# get the previous $s0
	lw $ra, 0($sp)		# get the return address
	addi $sp, $sp, 8	# adjust the stack pointer
	jr $ra			# return

