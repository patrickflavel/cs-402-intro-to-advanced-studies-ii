.data 0x10010000
var1: .word 4 		# var1 is a word (32 bit) with initial value of 4
limit: .word 100	# limit is a word (32 bit) with initial value 100

.text
.globl main
main: addu $s0, $ra, $0	# save $31 in $16

lw $t0, var1		# load word var1 in $t0, i.e. i is in $t0
lw $t1, limit		# load word limit in $t1

Loop:
ble $t1, $t0, Exit      # exit if limit <= i
addi $t0, $t0, 1	# i = i+1
sw $t0, var1		# store incremented var1 in var1
j Loop			# jump to label Loop
Exit:			# Exit label
			# restore now the return address in $ra and return from main
addu $ra, $0, $s0 	# return address back in $31
jr $ra 			# return from main
