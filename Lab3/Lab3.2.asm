.data 0x10010000
var1: .word 4 		# var1 is a word (32 bit) with initial value of 4
var2: .word 5 		# var2 is a word (32 bit) with initial value of 5 
var3: .word -2016	# var3 is a word (32 bit) with initial value of -2016

.text
.globl main
main: addu $s0, $ra, $0	# save $31 in $16

lw $t0, var1		# load word var1 in $t0
lw $t1, var2		# load word var2 in $t1
lw $t2, var3		# load word var3 in $t2

slt $t3, $t0, $t1	# if ($t0 < $t1) then $t3 = 1; else $t3 = 0;
bgtz $t3, Else		# if $t3 is greater than zero, jump to Else
beq $t0, $t1, Else	# if ($t0 = $t1) then jump to Else
sw $t2, var1		# store $t2 (var3) in var1
sw $t2, var2		# store $t2 (var3) in var2
beq $0, $0, Exit	# branch to Exit
Else:			# Else label
sw $t1, var1		# store $t1 (var2) in var1
sw $t0, var2		# store $t0 (var1) in var2
Exit:			# Exit label
			# restore now the return address in $ra and return from main
addu $ra, $0, $s0 	# return address back in $31
jr $ra 			# return from main
