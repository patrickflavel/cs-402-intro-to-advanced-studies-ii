.data 0x10010000
my_array: .space 40 	# my_array is an array of size 40 bytes = 10 elements of 32 bits each
initial_value: .word 4	# limit is a word (32 bit) with initial value 100
limit: .word 10

.text
.globl main
main: addu $s0, $ra, $0	# save $31 in $16

lw $t0, initial_value	# load word initial_value in $t0 (represents value of j)
lw $t1, limit		# load word limit in $t1
li $t2, 0		# load immediate value 0 in $t2 (represents i in for loop)
la $t3, my_array	# load address of my_array in $t3
li $t3, 0		# load immediate value 0 in $t3 (represents relative number of bytes to successive elements within my_array; initialized to zero)

Loop:
ble $t1, $t2, Exit      # exit if limit <= i
sw $t0, my_array($t3)	# store incremented j in my_array
addi $t0, $t0, 1	# j = j+1
addi $t2, $t2, 1	# i = i+1
addi $t3, $t3, 4	# array_relative_pointer = array_relative_pointer + 4 bytes
j Loop			# jump to label Loop
Exit:			# Exit label
			# restore now the return address in $ra and return from main
addu $ra, $0, $s0 	# return address back in $31
jr $ra 			# return from main
