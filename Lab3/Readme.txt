Author: Patrick Flavel
CWID: A20355341
Class: CS 402
Instructor: Virgil Bistriceanu
Lab#: 3
Date: 2/29/2016


Please see Lab3_Report.docx for answers to all questions.


***Pre-Lab***
Please see Lab3.1.asm and Lab3.2.asm.


***In-Lab***
Please see Lab3.3.asm.


***Post-Lab***
Please see Lab3.4.asm.